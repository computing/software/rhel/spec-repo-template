# Spec repository template

This repository is a [cookiecutter](https://cookiecutter.readthedocs.io)
template configuration to create a spec repository, including gitlab
continuous integration.

## Creating a new spec repository

To use this template to create a new repository in the current directory:

```
python -m cookiecutter https://git.ligo.org/packaging/rhel/spec-repo-template
```

This command will present two prompts, the first of which will be

```
package_name [mypackage]:
```

at this point insert the name of the package, which is presumed to match
the `%{name}` variable in the spec file, and the name of the spec file itself.

The second prompt looks like:

```
Select lint:
1 - yes
2 - no
Choose from 1, 2 (1, 2) [1]:
```

Only if you are modifying a third-party spec file, where linting errors are
likely not your fault, should you choose `2` (no linting).
In all other cases you should enter `1`, or just press return to select it
from the default, to include a call to `rpmlint` in the continuous
integration.

## Updating an existing spec repo to the latest template

To 'update' an existing repo to match the latest template, just
rerun the whole thing in the same directory, and commit that:

```
python -m cookiecutter https://git.ligo.org/packaging/rhel/spec-repo-template --overwrite-if-exists --output-dir ..
```
